{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nixos-generators = {
      url = "github:nix-community/nixos-generators";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    flake-parts = {
      url = "github:hercules-ci/flake-parts";
    };
  };

  outputs = inputs @ {
    self,
    nixpkgs,
    home-manager,
    nixos-generators,
    flake-parts,
    ...
  }:
    flake-parts.lib.mkFlake {inherit inputs;} {
      flake = {
        nixosModules = {
          git = import ./nixos_modules/git;
          gnome = import ./nixos_modules/gnome;
          locale = import ./nixos_modules/locale;
          micro_editor = import ./nixos_modules/micro_editor;
          nix = import ./nixos_modules/nix;
          security = import ./nixos_modules/security;
          ssh = import ./nixos_modules/ssh;
          swapfile = import ./nixos_modules/swapfile;
          vscode = import ./nixos_modules/vscode;
          zsh = import ./nixos_modules/zsh;
        };

        packages.x86_64-linux = {
          vmware_wks = nixos-generators.nixosGenerate {
            system = "x86_64-linux";
            modules = [
              self.nixosModules.locale
              self.nixosModules.swapfile
            ];
            format = "vmware";
          };
        };

        nixosConfigurations = {
          mock = nixpkgs.lib.nixosSystem {
            system = "x86_64-linux";

            modules =
              nixpkgs.lib.attrsets.attrVals (builtins.attrNames self.nixosModules) self.nixosModules
              ++ [
                home-manager.nixosModules.home-manager
                {
                  fileSystems."/" = {
                    fsType = "ext4";
                    device = "/dev/disk/by-partlabel/non-existent";
                  };
                  boot.loader.systemd-boot.enable = true;
                  system.stateVersion = "24.11";
                  home-manager.users.kamil.home.stateVersion = "24.11";
                  users.users.kamil = {
                    isNormalUser = true;
                  };
                }
              ];
          };
        };
      };

      systems = [
        "x86_64-linux"
        "aarch64-linux"
        # ...
      ];
      perSystem = {pkgs, ...}: {
        devShells = {
          default = pkgs.mkShell {
            nativeBuildInputs = with pkgs; [wget bat cargo];
          };
        };

        formatter = pkgs.alejandra;
      };
    };
}
