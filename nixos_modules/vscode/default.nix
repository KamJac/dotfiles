{pkgs, ...}: {
  home-manager.users.kamil = {
    programs.vscode = {
      # Enable vscode but set package to vscodium
      enable = true;
      package = pkgs.vscodium;

      profiles.default = {
        # Add theme extension
        extensions = with pkgs.vscode-extensions; [
          viktorqvarfordt.vscode-pitch-black-theme
        ];

        # Set theme and font
        userSettings = {
          "workbench.colorTheme" = "Pitch Black";
          "editor.fontFamily" = "'JetBrains Mono', 'monospace', monospace";
        };

        # Disable update checks as those are managed by nix
        enableUpdateCheck = false;
        enableExtensionUpdateCheck = false;
      };
    };

    # Add font package
    home.packages = with pkgs; [
      jetbrains-mono
    ];
  };
}
