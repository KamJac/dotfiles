{...}: {
    programs.ssh = {
        startAgent = true;
        extraConfig = "
            ServerAliveInterval 3
        ";
    };
}
