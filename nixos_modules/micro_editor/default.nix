{pkgs, ...}: {
  home-manager.users.kamil = {
    home.sessionVariables."EDITOR" = "micro";

    home.packages = with pkgs; [
      xclip
      xsel
      wl-clipboard
    ];

    programs.micro = {
      enable = true;
      settings = {
        autoindent = true;
        autosave = 0;
        autosu = true;
        backup = true;
        backupdir = "";
        basename = false;
        clipboard = "external";
        colorcolumn = 0;
        #colorscheme = "simple";
        colorscheme = "twilight";
        #colorscheme = "solarized";
        cursorline = true;
        detectlimit = 420;
        diffgutter = false;
        divchars = "|";
        divreverse = true;
        encoding = "utf-8";
        eofnewline = true;
        fakecursor = false;
        fastdirty = false;
        #fileformat
        #filetype
        helpsplit = "hsplit";
        hlsearch = true;
        hltaberrors = true;
        hltrailingws = true;
        incsearch = true;
        ignorecase = true;
        indentshar = " ";
        infobar = true;
        keepautoindent = false;
        keymenu = true;
        matchbrace = true;
        matchbraceleft = true;
        matchbracestyle = "highlight";
        mkparents = true;
        mouse = true;
        multiopen = "tab";
        pageoverlap = 3;
        paste = false;
        parsecursor = false;
        permbackup = false;
        pluginchannels = ["https://raw.githubusercontent.com/micro-editor/plugin-channel/master/channel.json"];
        pluginrepos = [""];
        #readonly
        reload = "prompt";
        rmtrailingws = true;
        ruler = true;
        relativeruler = false;
        savecursor = true;
        savehistory = true;
        saveundo = true;
        scrollbar = true;
        scrollbarchar = "|";
        scrollmargin = 3;
        scrollspeed = 3;
        smartpaste = true;
        softwrap = true;
        splitbottom = true;
        splitright = true;
        statusformatl = "$(filename) $(modified)(,$(col)) $(status.paste)";
        statusformatr = "$(bind:ToggleKeyMenu): bindings, $(bind:ToggleHelp): help";
        statusline = true;
        sucmd = "sudo";
        syntax = true;
        tabmovement = true;
        tabhighlight = false;
        tabreverse = true;
        tabsize = 4;
        tabstospaces = true;
        useprimary = true;
        wordwrap = true;
        xterm = false;

        # plugins
        autoclose = true;
        comment = true;
        ftoptions = true;
        linter = true;
        literate = false;
        status = true;
        diff = false;
      };
    };
  };
}
