{
  lib,
  pkgs,
  ...
}: {
  services.xserver = {
    enable = true;
    displayManager.gdm = {
      enable = true;
      autoSuspend = false;
    };

    desktopManager.gnome.enable = true;
  };

  services.gvfs.enable = lib.mkForce false;

  environment.gnome.excludePackages = with pkgs; [
    gnome-tour
    gnome-connections
    yelp # Helop viewer
    simple-scan
    geary # Mail client
    cheese # Photo booth
    totem # Movie player
    epiphany # Browser
    gnome-maps
    gnome-weather
    gnome-font-viewer
    gnome-logs
    gnome-characters
    gnome-clocks
    gnome-disk-utility
    gnome-music
    gnome-calendar
    gnome-contacts
    snapshot
  ];

  home-manager.users.kamil = {
    home.packages = with pkgs; [
      gnomeExtensions.gtile
    ];

    dconf = {
      enable = true;
      settings = {
        "org/gnome/mutter" = {
          dynamic-workspaces = true;
        };
        "org/gnome/shell" = {
          disable-user-extensions = false;
          enabled-extensions = with pkgs.gnomeExtensions; [
            gtile.extensionUuid
            system-monitor.extensionUuid
            native-window-placement.extensionUuid
          ];
        };
        "org/gnome/shell/extensions/gtile" = {
          grid-sizes = "2x1,8x4,16x8";
        };
        "org/gnome/desktop/interface" = {
          color-scheme = "prefer-dark";
          enable-hot-corners = false;
        };
        "org/gnome/desktop/screensaver" = {
          lock-enabled = false;
        };
        "org/gnome/desktop/session" = {
          idle-delay = 0;
        };
        "org/gnome/desktop/wm/preferences" = {
          action-middle-click-titlebar = "minimize";
        };
      };
    };
  };
}
