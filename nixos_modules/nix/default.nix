{
  pkgs,
  lib,
  ...
}: {
  nix = {
    settings = {
      substituters = [
        "https://cache.nixos.org"
        "https://nix-community.cachix.org"
        "https://alejandra.cachix.org"
      ];
      trusted-public-keys = [
        "cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY="
        "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
        "alejandra.cachix.org-1:NjZ8kI0mf4HCq8yPnBfiTurb96zp1TBWl8EC54Pzjm0="
      ];
      experimental-features = [
        "nix-command"
        "flakes"
      ];
      auto-optimise-store = true;
    };

    gc.automatic = true;
    optimise.automatic = true;
    daemonIOSchedClass = "idle";
    daemonCPUSchedPolicy = "idle";
  };

  nixpkgs.config = {
    allowUnfree = true;
    allowBroken = false;
  };

  system.stateVersion = lib.mkDefault "24.11";
  environment = {
    defaultPackages = [];

    systemPackages = with pkgs; [
      alejandra
      deadnix
    ];
  };
}
