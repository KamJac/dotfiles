{...}: {
  swapDevices = [
    {
      size = 1024 * 4;
      device = "/var/lib/swapfile";
      discardPolicy = "both";
    }
  ];
}
