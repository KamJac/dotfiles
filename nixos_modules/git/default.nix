{...}: {
  home-manager.users.kamil = {
    programs = {
      git = {
        # Enable git with my identity
        enable = true;
        userName = "Kamil Jacuński";
        userEmail = "kamilj@tutanota.com";

        # Enable caching of credentials
        extraConfig.credential = {
          helper = [
            "cache --timeout 21600"
          ];
        };

        # Enable difftastic with configuration and set alias
        difftastic = {
          enable = true;
          display = "side-by-side-show-both";
        };
        extraConfig.alias = {
          difft = "-c diff.external=difft diff";
        };
      };
    };
  };
}
