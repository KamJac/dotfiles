{...}: {
  networking.firewall = {
    enable = true;
    allowPing = false;
  };

  # Allow run0 to access pam
  security.pam.services.systemd-run0 = {};

  # Enable auto unlock of gnome keyring
  security.pam.services.gdm.enableGnomeKeyring = true;
}
