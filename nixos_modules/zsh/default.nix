{...}: {
  environment.pathsToLink = [
    "/share/zsh"
  ];
  programs.zsh.enable = true;

  home-manager.users.kamil = {
    programs.zsh = {
      enable = true;
      autosuggestion.enable = true;
      enableCompletion = true;
    };

    programs.starship = {
      enable = true;
      enableZshIntegration = true;
    };
  };
}
